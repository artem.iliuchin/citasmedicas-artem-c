﻿using citasMedica_artem_c.Models;
using Microsoft.EntityFrameworkCore;

namespace citasMedica_artem_c.Repository
{
    public class PacienteRepository
    {

        private readonly CitasContext _context;

        public PacienteRepository(CitasContext context)
        {

            _context = context;

        }

        public IEnumerable<Paciente> GetAll()
        {
            return _context.Pacientes
                .Include(p => p.Medicos)
                .Include(p => p.Citas)
                    .ThenInclude(c => c.Diagnosticos)
                .ToList();
        }

        public Paciente GetById(int id)
        {
            return _context.Pacientes
                .Include(p => p.Medicos)
                .Include(p => p.Citas)
                .FirstOrDefault(p => p.Id == id);
        }

        public Paciente GetUserByUsuario(string usuario, string clave)
        {
            var paciente = _context.Pacientes.FirstOrDefault(u => u.Usuario1 == usuario && u.Clave == clave);

            return paciente;
        }

        public void Add(Paciente paciente)
        {

            _context.Pacientes.Add(paciente);
            _context.SaveChanges();

        }

        public void Update(Paciente paciente)
        {
            
            _context.Entry(paciente).State = EntityState.Modified;
            _context.SaveChanges();

        }

        public void Delete(int id)
        {

            Paciente paciente = _context.Pacientes.Find(id);

            if(paciente != null)
            {
                _context.Pacientes.Remove(paciente);
                _context.SaveChanges();
            }

        }





    }
}
