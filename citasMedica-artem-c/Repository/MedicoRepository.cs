﻿using citasMedica_artem_c.Models;
using Microsoft.EntityFrameworkCore;


namespace citasMedica_artem_c.Repository

{
    public class MedicoRepository
    {

        private readonly CitasContext _context;


        public MedicoRepository(CitasContext context)
        {
            _context = context;
        }

        public IEnumerable<Medico> GetAll()
        {

            return _context.Medicos
                .Include(m => m.Citas)
                    .ThenInclude(c => c.Diagnosticos)
                .Include(m => m.Pacientes)
                .ToList();

        }


        public Medico GetById(int id)
        {

            return _context.Medicos
                .Include(m => m.Pacientes)
                .Include(m => m.Citas)
                .FirstOrDefault(m => m.Id == id);

        }

        public Medico GetUserByUsuario(string usuario, string clave)
        {
            var medico= _context.Medicos.FirstOrDefault(u => u.Usuario1 == usuario && u.Clave == clave);

            return medico;
        }

        public void Add(Medico medico)
        {

            _context.Medicos.Add(medico);
            _context.SaveChanges();

        }

        public void Update(Medico medico)
        {

            _context.Entry(medico).State = EntityState.Modified;
            _context.SaveChanges();

        }

        public void Delete(int id)
        {

            Medico medico = _context.Medicos.Find(id);
            if(medico != null)
            {
                _context.Medicos.Remove(medico);
                _context.SaveChanges();
            }

        }



    }
}
