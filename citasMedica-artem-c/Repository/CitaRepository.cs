﻿using AutoMapper;
using citasMedica_artem_c.Models;
using Microsoft.EntityFrameworkCore;

namespace citasMedica_artem_c.Repository
{
    public class CitaRepository
    {
        private readonly CitasContext _context;

        public CitaRepository(CitasContext context)
        {
            _context = context;
        }

        public IEnumerable<Cita> GetAll()
        {
            return _context.Citas
                .Include(c => c.Medico)
                .Include(c => c.Paciente)
                .Include(c => c.Diagnosticos)
                .ToList();
        }

        public Cita GetById(int id)
        {
            return _context.Citas
                .Include(c => c.Medico)
                .Include(c => c.Paciente)
                .FirstOrDefault(c => c.Id == id);
        }

        public void Add(Cita cita)
        {
            _context.Citas.Add(cita);
            _context.SaveChanges();
        }

        public void Update(Cita cita)
        {
            _context.Entry(cita).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Cita cita = _context.Citas.Find(id);
            if (cita != null)
            {
                _context.Citas.Remove(cita);
                _context.SaveChanges();
            }
        }
    }
}
