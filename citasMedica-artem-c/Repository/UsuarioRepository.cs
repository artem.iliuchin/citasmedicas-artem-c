﻿using citasMedica_artem_c.Models;
using Microsoft.EntityFrameworkCore;

namespace citasMedica_artem_c.Repository
{
    public class UsuarioRepository
    {
        private readonly CitasContext _context;

        public UsuarioRepository(CitasContext context)
        {
            _context = context;
        }

        public IEnumerable<Usuario> GetAll()
        {
            return _context.Usuarios.ToList();
        }

        public Usuario GetById(int id)
        {
            return _context.Usuarios.Find(id);
        }

        public Usuario GetUserByUsuario(string usuario, string clave)
        {
            var user = _context.Usuarios.FirstOrDefault(u => u.Usuario1 == usuario && u.Clave == clave);

            return user;
        }

        public Usuario Create(Usuario usuario)
        {
            _context.Usuarios.Add(usuario);
            _context.SaveChanges();
            return usuario;
        }

        public void Update(Usuario usuario)
        {
            _context.Entry(usuario).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var usuario = _context.Usuarios.Find(id);
            if (usuario != null)
            {
                _context.Usuarios.Remove(usuario);
                _context.SaveChanges();
            }
        }


    }
}
