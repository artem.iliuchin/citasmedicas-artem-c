﻿using citasMedica_artem_c.Models;
using Microsoft.EntityFrameworkCore;

namespace citasMedica_artem_c.Repository
{
    public class DiagnosticoRepository
    {

        private readonly CitasContext _context;

        public DiagnosticoRepository(CitasContext context)
        {
            _context = context;
        }


        public IEnumerable<Diagnostico> GetAll()
        {
            return _context.Diagnosticos
                .Include(d => d.Cita)
                    .ThenInclude(c => c.Medico)
                .Include(d => d.Cita)
                    .ThenInclude(c => c.Paciente)
                .ToList();
        }

        public Diagnostico GetById(int id)
        {
            return _context.Diagnosticos
                .Include(d => d.Cita)
                .FirstOrDefault(d => d.Id == id);
        }

        public void Add(Diagnostico diagnostico)
        {
            _context.Diagnosticos.Add(diagnostico);
            _context.SaveChanges();
        }

        public void Update(Diagnostico diagnostico)
        {
            _context.Entry(diagnostico).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Diagnostico diagnostico = _context.Diagnosticos.Find(id);
            if(diagnostico != null)
            {
                _context.Diagnosticos.Remove(diagnostico);
                _context.SaveChanges();
            }
        }


    }
}
