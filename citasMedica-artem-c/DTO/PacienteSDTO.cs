﻿using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.DTO
{
    public class PacienteSDTO: UsuarioDTO
    {

        public string Nss { get; set; } = null!;

        public string NumTarjeta { get; set; } = null!;

        public string Telefono { get; set; } = null!;

        public string Direccion { get; set; } = null!;

        public PacienteSDTO() { }

        public PacienteSDTO(Paciente paciente) 
        {
            Nss = paciente.Nss;
            NumTarjeta = paciente.NumTarjeta;
            Telefono = paciente.Telefono;
            Direccion = paciente.Direccion;

        }

    }
}
