﻿using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.DTO
{
    public class UsuarioDTO
    {
        public int Id { get; set; }

        public string Nombre { get; set; } = null!;

        public string Apellidos { get; set; } = null!;

        public string Usuario1 { get; set; } = null!;

        public string Clave { get; set; } = null!;

        public UsuarioDTO() { }

        public UsuarioDTO(Usuario usuario)
        {
            Id = usuario.Id;
            Nombre = usuario.Nombre;
            Apellidos = usuario.Apellidos;
            Usuario1 = usuario.Usuario1;
            Clave = usuario.Clave;
        }
    }
}
