﻿using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.DTO
{
    public class DiagnosticoSDTO
    {

        public int Id { get; set; }
        public string ValoracionEspecialista { get; set; }
        public string Enfermedad { get; set; }

        public DiagnosticoSDTO() { }

        public DiagnosticoSDTO(Diagnostico diagnostico)
        {
            Id = diagnostico.Id;
            ValoracionEspecialista = diagnostico.ValoracionEspecialista;
            Enfermedad = diagnostico.Enfermedad;
        }

    }
}
