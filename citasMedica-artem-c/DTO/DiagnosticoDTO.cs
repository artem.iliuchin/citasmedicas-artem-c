﻿using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.DTO
{
    public class DiagnosticoDTO
    {
        public int Id { get; set; }
        public string ValoracionEspecialista { get; set; }
        public string Enfermedad { get; set; }

        public CitaDTO Cita { get; set; }

        public DiagnosticoDTO() { }
        
        public DiagnosticoDTO(Diagnostico diagnostico)
        {
            Id = diagnostico.Id;
            ValoracionEspecialista = diagnostico.ValoracionEspecialista;
            Enfermedad = diagnostico.Enfermedad;
            Cita = new CitaDTO(diagnostico.Cita);
        }
    }
}
