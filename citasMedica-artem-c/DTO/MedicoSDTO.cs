﻿using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.DTO
{
    public class MedicoSDTO : UsuarioDTO
    {
        public string NumColegiado { get; set; } = null!;


        public MedicoSDTO() { }

        public MedicoSDTO(Medico medico) : base(medico)
        {
            NumColegiado = medico.NumColegiado;

        }
    }
}
