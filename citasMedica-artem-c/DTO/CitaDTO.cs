﻿using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.DTO
{
    public class CitaDTO
    {
        public int Id { get; set; }

        public DateTime? FechaHora { get; set; }

        public string? MotivoCita { get; set; }

        public ICollection<DiagnosticoSDTO> Diagnosticos { get; set; } = new List<DiagnosticoSDTO>();

        public MedicoSDTO Medico { get; set; }

        public PacienteSDTO Paciente { get; set; }

        public CitaDTO() { }

        public CitaDTO(Cita cita)
        {
            Id = cita.Id;
            FechaHora = cita.FechaHora;
            MotivoCita = cita.MotivoCita;
            Medico = new MedicoSDTO(cita.Medico);
            Paciente = new PacienteSDTO(cita.Paciente);

            foreach (var diagnostico in cita.Diagnosticos)
            {
                Diagnosticos.Add(new DiagnosticoSDTO(diagnostico));
            }
        }
    }
}
