﻿using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.DTO
{
    public class MedicoDTO : UsuarioDTO
    {
        public string NumColegiado { get; set; } = null!;

        public ICollection<CitaDTO> Citas { get; set; } = new List<CitaDTO>();

        public ICollection<PacienteSDTO> Pacientes { get; set; } = new List<PacienteSDTO>();

        public MedicoDTO() { }

        public MedicoDTO(Medico medico) : base(medico)
        {
            NumColegiado = medico.NumColegiado;

            foreach (var cita in medico.Citas)
            {
                Citas.Add(new CitaDTO(cita));
            }

            foreach (var paciente in medico.Pacientes)
            {
                Pacientes.Add(new PacienteSDTO(paciente));
            }
        }
    }
}
