﻿using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.DTO
{
    public class PacienteDTO : UsuarioDTO
    {
        public string Nss { get; set; } = null!;

        public string NumTarjeta { get; set; } = null!;

        public string Telefono { get; set; } = null!;

        public string Direccion { get; set; } = null!;

        public ICollection<CitaDTO> Citas { get; set; } = new List<CitaDTO>();

        public ICollection<MedicoSDTO> Medicos { get; set; } = new List<MedicoSDTO>();

        public PacienteDTO() { }

        public PacienteDTO(Paciente paciente) : base(paciente)
        {
            Nss = paciente.Nss;
            NumTarjeta = paciente.NumTarjeta;
            Telefono = paciente.Telefono;
            Direccion = paciente.Direccion;

            foreach (var cita in paciente.Citas)
            {
                Citas.Add(new CitaDTO(cita));
            }

            foreach (var medico in paciente.Medicos)
            {
                Medicos.Add(new MedicoSDTO(medico));
            }
        }
    }
}
