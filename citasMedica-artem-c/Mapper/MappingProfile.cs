﻿using AutoMapper;
using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Models;

namespace citasMedica_artem_c.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Usuario, UsuarioDTO>();
            CreateMap<UsuarioDTO, Usuario>();

            CreateMap<Paciente, PacienteDTO>();
            CreateMap<PacienteDTO, Paciente>();

            CreateMap<Medico, MedicoDTO>();
            CreateMap<MedicoDTO, Medico>();

            CreateMap<Paciente, PacienteSDTO>();
            CreateMap<PacienteSDTO, Paciente>();

            CreateMap<Medico, MedicoSDTO>();
            CreateMap<MedicoSDTO, Medico>();

            CreateMap<Cita, CitaDTO>();
            CreateMap<CitaDTO, Cita>();

            CreateMap<Diagnostico, DiagnosticoDTO>();
            CreateMap<DiagnosticoDTO, Diagnostico>();

            CreateMap<Diagnostico, DiagnosticoSDTO>();
            CreateMap<DiagnosticoSDTO, Diagnostico>();
        }
    }
}
