﻿using AutoMapper;
using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Models;
using citasMedica_artem_c.Repository;

namespace citasMedica_artem_c.Service
{
    public class MedicoService
    {
        private readonly MedicoRepository _medicoRepository;
        private readonly IMapper _mapper;

        public MedicoService(MedicoRepository medicoRepository, IMapper mapper)
        {
            _medicoRepository = medicoRepository;
            _mapper = mapper;
        }

        public IEnumerable<MedicoDTO> GetAll()
        {

            var medicos = _medicoRepository.GetAll();
            return _mapper.Map<IEnumerable<MedicoDTO>>(medicos);

        }

        public MedicoDTO GetById(int id)
        {

            var medico = _medicoRepository.GetById(id);
            return _mapper.Map<MedicoDTO>(medico);

        }

        public MedicoDTO GetUserByUsername(string usuario, string clave)
        {
            var medico = _medicoRepository.GetUserByUsuario(usuario, clave);
            return _mapper.Map<MedicoDTO>(medico);
        }


        public MedicoDTO Add(MedicoDTO medicoDTO)
        {

            var medico = _mapper.Map<Medico>(medicoDTO);
            _medicoRepository.Add(medico);

            var createdMedicoDTO = _mapper.Map<MedicoDTO>(medico);

            return createdMedicoDTO;

        }

        public void Update(MedicoDTO medicoDTO)
        {

            var medico = _mapper.Map<Medico>(medicoDTO);
            _medicoRepository.Update(medico);

        }

        public void Delete(int id)
        {
            _medicoRepository.Delete(id);
        }



    }
}
