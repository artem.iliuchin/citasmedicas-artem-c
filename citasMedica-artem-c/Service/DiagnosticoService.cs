﻿using AutoMapper;
using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Models;
using citasMedica_artem_c.Repository;

namespace citasMedica_artem_c.Service
{
    public class DiagnosticoService
    {

        private readonly DiagnosticoRepository _diagnosticoRepository;
        private readonly IMapper _mapper;

        public DiagnosticoService(DiagnosticoRepository diagnosticoRepository, IMapper mapper)
        {
            _diagnosticoRepository = diagnosticoRepository;
            _mapper = mapper;
        }

        public IEnumerable<DiagnosticoDTO> GetAll()
        {

            var diagnosticos = _diagnosticoRepository.GetAll();
            return _mapper.Map<IEnumerable<DiagnosticoDTO>>(diagnosticos);

        }

        public DiagnosticoDTO GetById(int id)
        {

            var diagnostico = _diagnosticoRepository.GetById(id);
            return _mapper.Map<DiagnosticoDTO>(diagnostico);

        }

        public DiagnosticoDTO Add(DiagnosticoDTO diagnosticoDTO)
        {

            var diagbostico = _mapper.Map<Diagnostico>(diagnosticoDTO);
            _diagnosticoRepository.Add(diagbostico);

            var createdDiagnosticoDTO = _mapper.Map<DiagnosticoDTO>(diagbostico);
            return createdDiagnosticoDTO;

        }

        public void Update(DiagnosticoDTO diagnosticoDTO)
        {

            var diagnostico = _mapper.Map<Diagnostico>(diagnosticoDTO);
            _diagnosticoRepository.Update(diagnostico);

        }

        public void Delete(int id)
        {

            _diagnosticoRepository.Delete(id);

        }

    }
}
