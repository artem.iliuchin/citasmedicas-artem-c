﻿using AutoMapper;
using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Models;
using citasMedica_artem_c.Repository;

namespace citasMedica_artem_c.Service
{
    public class PacienteService
    {

        private readonly PacienteRepository _pacienteRepository;
        private readonly IMapper _mapper;

        public PacienteService(PacienteRepository pacienteRepository, IMapper mapper)
        {
            _pacienteRepository = pacienteRepository;
            _mapper = mapper;
        }

        public IEnumerable<PacienteDTO> GetAll()
        {
            
            var pacientes = _pacienteRepository.GetAll();
            return _mapper.Map<IEnumerable<PacienteDTO>>(pacientes);

        }

        public PacienteDTO GetById(int id)
        {

            var paciente = _pacienteRepository.GetById(id);
            return _mapper.Map<PacienteDTO>(paciente);

        }

        public PacienteDTO GetUserByUsername(string usuario, string clave)
        {
            var paciente = _pacienteRepository.GetUserByUsuario(usuario, clave);
            return _mapper.Map<PacienteDTO>(paciente);
        }

        public PacienteDTO Add(PacienteDTO pacienteDTO)
        {

            var paciente = _mapper.Map<Paciente>(pacienteDTO);
            _pacienteRepository.Add(paciente);

            var createdPacienteDTO = _mapper.Map<PacienteDTO>(paciente);

            return createdPacienteDTO;

        }

        public void Update(PacienteDTO pacienteDTO)
        {

            var paciente = _mapper.Map<Paciente>(pacienteDTO);
            _pacienteRepository.Update(paciente);

        }

        public void Delete(int id)
        {
            _pacienteRepository.Delete(id);
        }



    }
}
