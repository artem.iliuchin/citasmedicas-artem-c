﻿using AutoMapper;
using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Models;
using citasMedica_artem_c.Repository;

namespace citasMedica_artem_c.Service
{
    public class CitaService
    {
        private readonly CitaRepository _citaRepository;
        private readonly IMapper _mapper;

        public CitaService(CitaRepository citaRepository, IMapper mapper)
        {
            _citaRepository = citaRepository;
            _mapper = mapper;
        }

        public IEnumerable<CitaDTO> GetAll()
        {
            var citas = _citaRepository.GetAll();
            return _mapper.Map<IEnumerable<CitaDTO>>(citas);
        }

        public CitaDTO GetById(int id)
        {
            var cita = _citaRepository.GetById(id);
            return _mapper.Map<CitaDTO>(cita);
        }

        public CitaDTO Add(CitaDTO citaDTO)
        {
            var cita = _mapper.Map<Cita>(citaDTO);
            _citaRepository.Add(cita);

            var createdCitaDTO = _mapper.Map<CitaDTO>(cita);

            return createdCitaDTO;
        }

        public void Update(CitaDTO citaDTO)
        {
            var cita = _mapper.Map<Cita>(citaDTO);
            _citaRepository.Update(cita);
        }

        public void Delete(int id)
        {
            _citaRepository.Delete(id);
        }
    }
}
