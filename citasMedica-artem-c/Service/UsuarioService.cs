﻿using AutoMapper;
using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Models;
using citasMedica_artem_c.Repository;

namespace citasMedica_artem_c.Service
{
    public class UsuarioService
    {

        private readonly UsuarioRepository _usuarioRepository;
        private readonly IMapper _mapper;

        public UsuarioService(UsuarioRepository usuarioRepository, IMapper mapper)
        {
            _usuarioRepository = usuarioRepository;
            _mapper = mapper;
        }

        public IEnumerable<UsuarioDTO> GetAll()
        {
            var usuarios = _usuarioRepository.GetAll();
            return _mapper.Map<IEnumerable<UsuarioDTO>>(usuarios);
        }

        public UsuarioDTO GetById(int id)
        {
            var usuario = _usuarioRepository.GetById(id);
            return _mapper.Map<UsuarioDTO>(usuario);
        }

        public UsuarioDTO GetUserByUsername(string usuario, string clave)
        {
            var Usuario = _usuarioRepository.GetUserByUsuario(usuario, clave);
            return _mapper.Map<UsuarioDTO>(Usuario);
        }

        public UsuarioDTO Create(Usuario usuario)
        {
            var Usuario = _usuarioRepository.Create(usuario);
            return _mapper.Map<UsuarioDTO>(Usuario);
        }

        public void Update(Usuario usuario)
        {
            _usuarioRepository.Update(usuario);
        }

        public void Delete(int id)
        {
            _usuarioRepository.Delete(id);
        }


    }
}
