﻿using System;
using System.Collections.Generic;

namespace citasMedica_artem_c.Models;

public partial class Usuario
{
    public int Id { get; set; }
    public string Nombre { get; set; }
    public string Apellidos { get; set; }
    public string Usuario1 { get; set; }
    public string Clave { get; set; }
}
