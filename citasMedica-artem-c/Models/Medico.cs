﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace citasMedica_artem_c.Models;

public partial class Medico:Usuario
{

    public string NumColegiado { get; set; }
    public virtual ICollection<Cita> Citas { get; set; }
    public virtual ICollection<Paciente> Pacientes { get; set; }
}
