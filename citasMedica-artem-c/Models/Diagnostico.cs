﻿using System;
using System.Collections.Generic;

namespace citasMedica_artem_c.Models;

public partial class Diagnostico
{
    public int Id { get; set; }

    public string ValoracionEspecialista { get; set; }

    public string Enfermedad { get; set; }

    public int CitaId { get; set; }

    public virtual Cita Cita { get; set; }
}
