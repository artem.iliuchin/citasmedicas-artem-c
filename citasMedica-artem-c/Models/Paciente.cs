﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace citasMedica_artem_c.Models;

public partial class Paciente:Usuario
{

    public string Nss { get; set; }
    public string NumTarjeta { get; set; }
    public string Telefono { get; set; }
    public string Direccion { get; set; }
    public virtual ICollection<Cita> Citas { get; set; }
    public virtual ICollection<Medico> Medicos { get; set; }
}
