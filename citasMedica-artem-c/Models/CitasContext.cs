﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace citasMedica_artem_c.Models;

public partial class CitasContext : DbContext
{
    public CitasContext()
    {
    }

    public CitasContext(DbContextOptions<CitasContext> options)
        : base(options)
    {
    }

    public DbSet<Usuario> Usuarios { get; set; }
    public DbSet<Medico> Medicos { get; set; }
    public DbSet<Paciente> Pacientes { get; set; }
    public DbSet<Cita> Citas { get; set; }
    public DbSet<Diagnostico> Diagnosticos { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=citas;Trusted_Connection=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Usuario>(entity =>
        {
            entity.ToTable("USUARIOS");

            entity.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("ID");
            entity.Property(e => e.Nombre)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("NOMBRE");
            entity.Property(e => e.Apellidos)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("APELLIDOS");
            entity.Property(e => e.Usuario1)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("USUARIO");
            entity.Property(e => e.Clave)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("CLAVE");
        });

        modelBuilder.Entity<Medico>(entity =>
        {
            entity.ToTable("MEDICOS");

            entity.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("ID");
            entity.Property(e => e.NumColegiado)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("NUM_COLEGIADO");

            entity.HasMany(d => d.Pacientes).WithMany(p => p.Medicos)
                .UsingEntity<Dictionary<string, object>>(
                    "MedicosPaciente",
                    r => r.HasOne<Paciente>().WithMany()
                        .HasForeignKey("PacienteId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_PACIENTES_MEDICOS_PACIENTE_ID"),
                    l => l.HasOne<Medico>().WithMany()
                        .HasForeignKey("MedicoId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_MEDICOS_PACIENTES_MEDICO_ID"),
                    j =>
                    {
                        j.HasKey("MedicoId", "PacienteId").HasName("MEDICOS_PACIENTES_PK");
                        j.ToTable("MEDICOS_PACIENTES");
                        j.Property<int>("MedicoId").HasColumnName("MEDICO_ID");
                        j.Property<int>("PacienteId").HasColumnName("PACIENTE_ID");
                    });
        });

        modelBuilder.Entity<Paciente>(entity =>
        {
            entity.ToTable("PACIENTES");

            entity.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("ID");
            entity.Property(e => e.Nss)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("NSS");
            entity.Property(e => e.NumTarjeta)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("NUM_TARJETA");
            entity.Property(e => e.Telefono)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("TELEFONO");
            entity.Property(e => e.Direccion)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("DIRECCION");
        });

        modelBuilder.Entity<Cita>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__CITAS__3214EC27F6B3691D");

            entity.ToTable("CITAS");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.FechaHora)
                .HasColumnType("datetime")
                .HasColumnName("FECHA_HORA");
            entity.Property(e => e.MedicoId).HasColumnName("MEDICO_ID");
            entity.Property(e => e.MotivoCita)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("MOTIVO_CITA");
            entity.Property(e => e.PacienteId).HasColumnName("PACIENTE_ID");

            entity.HasOne(d => d.Medico).WithMany(p => p.Citas)
                .HasForeignKey(d => d.MedicoId)
                .HasConstraintName("FK_CITAS_MEDICO_ID");

            entity.HasOne(d => d.Paciente).WithMany(p => p.Citas)
                .HasForeignKey(d => d.PacienteId)
                .HasConstraintName("FK_CITAS_PACIENTE_ID");
        });

        modelBuilder.Entity<Diagnostico>(entity =>
        {
            entity.ToTable("DIAGNOSTICOS");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.Enfermedad)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("ENFERMEDAD");
            entity.Property(e => e.ValoracionEspecialista)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("VALORACION_ESPECIALISTA");
            entity.Property(e => e.CitaId).HasColumnName("CITA_ID");

            entity.HasOne(d => d.Cita).WithMany(p => p.Diagnosticos)
                .HasForeignKey(d => d.CitaId)
                .HasConstraintName("FK_DIAGNOSTICOS_CITA_ID");
        });



        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
