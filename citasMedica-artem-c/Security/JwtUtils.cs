﻿using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Models;
using citasMedica_artem_c.Service;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace citasMedica_artem_c.Security
{
    public class JwtUtils
    {

        private readonly IConfiguration _configuration;
        private readonly MedicoService _medicoService;
        private readonly PacienteService _pacienteService;

        public JwtUtils(IConfiguration configuration, PacienteService pacienteService,MedicoService medicoService)
        {
            _configuration = configuration;
            _pacienteService = pacienteService;
            _medicoService = medicoService;
        }

        public string GenerateJwtToken(UsuarioDTO user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("Jwt:Key"));
            var audience = _configuration.GetValue<string>("Jwt:Audience");
            var issuer = _configuration.GetValue<string>("Jwt:Issuer");
            var expires = DateTime.UtcNow.AddDays(_configuration.GetValue<int>("Jwt:Expires"));

            string userRole = GetUserRole(user);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                new Claim("UserId", user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Usuario1),
                new Claim(ClaimTypes.Role, userRole)
            }),
                Expires = expires,
                Audience = audience,
                Issuer = issuer,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                                                            SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }


        private string GetUserRole(UsuarioDTO usuario)
        {
            string role = "Usuario";
            if(_medicoService.GetUserByUsername(usuario.Usuario1,usuario.Clave) != null)
            {
                role = "Medico";
            }else if(_pacienteService.GetUserByUsername(usuario.Usuario1,usuario.Clave) != null)
            {
                role = "Paciente";
            }

            return role;
        }
    }
}
