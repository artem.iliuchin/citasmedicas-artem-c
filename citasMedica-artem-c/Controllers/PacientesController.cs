﻿using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace citasMedica_artem_c.Controllers
{
    [ApiController]
    [Authorize(Roles = "Medico,Paciente")]
    [Route("[controller]")]
    public class PacientesController : ControllerBase
    {

        private readonly PacienteService _pacienteService;

        public PacientesController(PacienteService pacienteService)
        {
            _pacienteService = pacienteService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<PacienteDTO>> GetPacientes()
        {

            return Ok(_pacienteService.GetAll());

        }

        [HttpGet("{id}")]
        public ActionResult<PacienteDTO> GetPaciente(int id)
        {
            var paciente = _pacienteService.GetById(id);

            if (paciente == null)
            {

                return NotFound();

            }

            return Ok(paciente);

        }

        [HttpPost]

        public ActionResult<PacienteDTO> CreatePaciente(PacienteDTO pacienteDTO)
        {

            try
            {

                var paciente = _pacienteService.Add(pacienteDTO);
                return CreatedAtAction(nameof(GetPaciente), new { id = paciente.Id }, paciente);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPut]
        public IActionResult UpdatePaciente(PacienteDTO pacienteDTO)
        {

            try
            {
                _pacienteService.Update(pacienteDTO);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpDelete("{id}")]
        public IActionResult DeletePaciente(int id)
        {

            try
            {
                _pacienteService.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

    }
}
