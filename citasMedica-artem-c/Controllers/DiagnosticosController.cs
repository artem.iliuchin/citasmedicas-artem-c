﻿using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace citasMedica_artem_c.Controllers
{
    [ApiController]
    [Authorize(Roles = "Medico,Paciente")]
    [Route("[controller]")]
    public class DiagnosticosController :ControllerBase
    {

        private readonly DiagnosticoService _diagnosticoService;

        public DiagnosticosController(DiagnosticoService diagnosticoService)
        {
            _diagnosticoService = diagnosticoService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<DiagnosticoDTO>> Diagnosticos()
        {
            return Ok(_diagnosticoService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<DiagnosticoDTO> GetDiagnostico(int id)
        {
            var diagnostico = _diagnosticoService.GetById(id);

            if (diagnostico == null)
            {
                return NotFound();
            }

            return Ok(diagnostico);
        }

        [HttpPost]
        public ActionResult<DiagnosticoDTO> CreateDiagnostico(DiagnosticoDTO diagnosticoDTO)
        {
            try
            {
                var diagnostico = _diagnosticoService.Add(diagnosticoDTO);
                return CreatedAtAction(nameof(GetDiagnostico), new { id = diagnostico.Id }, diagnostico);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateDiagnostico(int id, DiagnosticoDTO diagnosticoDTO)
        {
            if (id != diagnosticoDTO.Id)
            {
                return BadRequest("Diagnostico ID mismatch.");
            }

            var diagnostico = _diagnosticoService.GetById(id);
            if (diagnostico == null)
            {
                return NotFound();
            }

            try
            {
                _diagnosticoService.Update(diagnosticoDTO);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteDiagnostico(int id)
        {
            var diagnostico = _diagnosticoService.GetById(id);
            if (diagnostico == null)
            {
                return NotFound();
            }

            try
            {
                _diagnosticoService.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

    }

}
