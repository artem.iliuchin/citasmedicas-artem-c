﻿using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace citasMedica_artem_c.Controllers
{
    [ApiController]
    [Authorize(Roles = "Medico,Paciente")]
    [Route("[controller]")]
    public class CitasController : ControllerBase
    {

        private readonly CitaService _citaService;

        public CitasController(CitaService citaService)
        {
            _citaService = citaService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CitaDTO>> GetCitas()
        {
            return Ok(_citaService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<CitaDTO> GetCita(int id)
        {
            var cita = _citaService.GetById(id);

            if (cita == null)
            {
                return NotFound();
            }

            return Ok(cita);
        }

        [HttpPost]
        public ActionResult<CitaDTO> CreateCita(CitaDTO citaDTO)
        {
            try
            {
                var createdCita = _citaService.Add(citaDTO);
                return CreatedAtAction(nameof(GetCita), new { id = createdCita.Id }, createdCita);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCita(CitaDTO citaDTO)
        {
            try
            {

                _citaService.Update(citaDTO);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteCita(int id)
        {
            try
            {
                _citaService.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

    }
}
