﻿using citasMedica_artem_c.DTO;
using citasMedica_artem_c.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace citasMedica_artem_c.Controllers
{
    [ApiController]
    [Authorize(Roles = "Medico")]
    [Route("[controller]")]
    public class MedicosController : ControllerBase
    {

        private readonly MedicoService _medicoService;

        public MedicosController(MedicoService medicoService)
        {
            _medicoService = medicoService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MedicoDTO>> GetMedicos()
        {

            return Ok(_medicoService.GetAll());

        }

        [HttpGet("{id}")]
        public ActionResult<MedicoDTO> GetMedico(int id)
        {
            var medico = _medicoService.GetById(id);

            if(medico == null)
            {

                return NotFound();

            }

            return Ok(medico);

        }

        [HttpPost]

        public ActionResult<MedicoDTO> CreateCita(MedicoDTO medicoDTO)
        {

            try
            {

                var medico = _medicoService.Add(medicoDTO);
                return CreatedAtAction(nameof(GetMedico),new { id = medico.Id }, medico);

            }catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPut]
        public IActionResult UpdateMedico(MedicoDTO medicoDTO)
        {

            try
            {
                _medicoService.Update(medicoDTO);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteMedico(int id)
        {

            try
            {
                _medicoService.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }


    }
}
